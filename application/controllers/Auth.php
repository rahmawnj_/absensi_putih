<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model(['admin_model', 'teacher_model']);
    }

    public function index()
    {
        if ($this->session->userdata('status')) {
            redirect('dashboard');
        }
        $data['title'] = 'Login';
        $this->load->view('auth/login', $data);
    }


    public function login()
    {
        if ($this->session->userdata('status')) {
            redirect('dashboard');
        }
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {


            $this->index();
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $admin = $this->db->get_where('admins', ['username' => $username])->row_array();;
            $teacher = $this->db->get_where('teachers', ['username' => $username])->row_array();;
            if ($admin) {
                $user = $admin;
            } else if ($teacher) {
                $user = $teacher;
            }
            if ($user) {
                if (password_verify($password, $user['password'])) {
                    if ($admin) {
                        $this->session->set_userdata([
                            'id_admin' => $user['id_admin'],
                            'username' => $user['username'],
                            'name' => $user['name'],
                            'photo' => $user['photo'],
                            'role' => 'admin'
                        ]);
                    } else {
                        $this->session->set_userdata([
                            'id_teacher' => $user['id_teacher'],
                            'username' => $user['username'],
                            'name' => $user['full_name'],
                            'photo' => $user['photo'],
                            'role' => 'teacher'
                        ]);
                    }

                    redirect('dashboard');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
						<div class="alert-message">
						Password Salah!
						</div>
					</div>');
                    redirect('auth/login');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
				<div class="alert-message">
				Username tidak terdaftar!
				</div>
			</div>');
                redirect('auth/login');
            }
        }
    }

    public function logout()
    {

        $this->session->unset_userdata(['username', 'name', 'photo', 'status', 'gambar']);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
				Berhasil Logout!
			  </div>');
        redirect('/');
    }

    public function profile()
    {
        // Cek status pengguna

        // Ambil data pengguna berdasarkan rolenya
        if ($this->session->userdata('role') == 'admin') {
            $user = $this->db->get_where('admins', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        } else {
            $user = $this->db->get_where('teachers', ['id_teacher' => $this->session->userdata('id_teacher')])->row_array();
        }

        $this->load->view('auth/profile', ['user' => $user, 'title' => 'Profile']);
    }

    public function updateProfile($id)
    {
        // Cek role pengguna
        $role = $this->session->userdata('role');

        if ($role == 'admin') {
            $this->updateAdminProfile($id);
        } elseif ($role == 'teacher') {
            $this->updateTeacherProfile($id);
        } else {
            // Role tidak valid, berikan respons sesuai kebutuhan
        }
    }

    public function updateAdminProfile($id)
    {
        $admin = $this->admin_model->get_by_id($id);
        if ($this->input->post('username') == $admin['username']) {
            $username_rules = 'required|trim';
        } else {
            $username_rules = 'required|trim|is_unique[admins.username]';
        }
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', $username_rules);

        if ($this->form_validation->run() == false) {
            // Jika validasi gagal, tampilkan kembali form dengan pesan error
            $this->profile(); // Mengambil data pengguna berdasarkan ID
        } else {

            if (!empty($_FILES['photo']['name'])) {
                // Hapus foto profil lama
                $old_photo = $admin['photo'];
                if ($old_photo != 'default_photo.jpg') {
                    unlink('./assets/uploads/img/' . $old_photo);
                }

                // Unggah foto profil yang baru
                $photo_path = '';
                $upload_path = './assets/uploads/img/admins/';

                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);

                $photo = 'admins/' . $photo_path;
            } else {
                $photo = $admin['photo'];
            }


            if ($this->input->post('password') == '') {
                $password = $admin['password'];
            } else {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            $data = array(
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'photo' => $photo,
                'password' => $password,
            );

            if ($this->admin_model->update($id, $data)) {
                $this->session->set_userdata([
                    'id_admin' => $id,
                    'username' => $this->input->post('username'),
                    'name' => $this->input->post('name'),
                    'photo' => $photo,
                    'role' => 'admin'
                ]);
                $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
            }
            redirect('auth/profile');
        }
    }

    public function updateTeacherProfile($id)
    {
        // Validasi form untuk role teacher
        $teacher = $this->teacher_model->get_by_id($id);
        if ($this->input->post('username') == $teacher['username']) {
            $username_rules = 'required|trim';
        } else {
            $username_rules = 'required|trim|is_unique[teachers.username]';
        }
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', $username_rules);
        $this->form_validation->set_rules('nuptk', 'NUPTK', 'required|trim|numeric');
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'required');
        $this->form_validation->set_rules('birth_place', 'Birth Place', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('degree', 'Degree', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $this->profile(); 
        } else {
            if (!empty($_FILES['photo']['name'])) {
                $old_photo = $teacher['photo'];
                if ($old_photo != 'default_photo.jpg') {
                    unlink('./assets/uploads/img/' . $old_photo);
                }
                $photo_path = '';
                $upload_path = './assets/uploads/img/teachers/';

                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);
                $photo = 'teachers/' . $photo_path;
            } else {
                $photo = $teacher['photo'];
            }

            if ($this->input->post('password') == '') {
                $password = $teacher['password'];
            } else {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            $data = array(
                'full_name' => $this->input->post('full_name'),
                'nuptk' => $this->input->post('nuptk'),
                'username' => $this->input->post('username'),
                'photo' => $photo,
                'password' => $password,
                'birth_date' => $this->input->post('birth_date'),
                'birth_place' => $this->input->post('birth_place'),
                'gender' => $this->input->post('gender'),
                'degree' => $this->input->post('degree'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_number'),
            );

            if ($this->teacher_model->update($id, $data)) {
                $this->session->set_userdata([
                    'id_teacher' => $id,
                    'username' => $this->input->post('username'),
                    'name' => $this->input->post('full_name'),
                    'photo' => $photo,
                    'role' => 'teacher'
                ]);
                $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
            }
            redirect('auth/profile');
        }
    }

    public function validatePhoto($str)
    {
        // Cek apakah file foto diupload
        if ($_FILES['photo']['name']) {
            $allowedTypes = ['jpg', 'jpeg', 'png'];
            $fileExt = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);

            // Cek tipe file
            if (!in_array($fileExt, $allowedTypes)) {
                $this->form_validation->set_message('validatePhoto', 'Tipe file foto tidak valid. Harap upload file dengan format JPG, JPEG, atau PNG.');
                return false;
            }
        }

        return true;
    }
}
