<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Mpdf\Mpdf;

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model(['admin_model', 'class_model', 'subject_model', 'holiday_model', 'teacher_model', 'student_model']);
        $this->load->helper(['form', 'url',]);
        if (!$this->session->userdata('username')) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible" role="alert">
            <div class="alert-message">
            Login terlebih dahulu!
            </div>
        </div>');
            redirect('auth/login');
        }
    }

    public function index()
    {
        $this->load->view('page/dashboard/index', [
            'title' => 'Dashboard',
        ]);
    }

    public function admin_index()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $this->load->view('page/dashboard/admin/index', [
            'title' => 'Admin',
            'admins' => $this->admin_model->get_all()
        ]);
    }

    public function admin_create()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/admin/create', [
            'title' => 'Admin'
        ]);
    }

    public function admin_store()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[admins.username]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == false) {
            $this->admin_create();
        } else {
            $photo_path = '';
            $upload_path = './assets/uploads/img/admins/';

            if (!empty($_FILES['photo']['name'])) {
                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);
            }

            $data = array(
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'photo' => 'admins/' . $photo_path,
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            );
            if ($this->admin_model->insert($data)) {
                $this->session->set_flashdata('success', 'User Berhasil Ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'User Berhasil Ditambahkan!');
            }
            redirect('dashboard/admin_index');
        }
    }

    public function admin_edit($id)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/admin/edit', [
            'title' => 'Admin',
            'admin' => $this->admin_model->get_by_id($id)
        ]);
    }

    public function admin_update($id_admin)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $admin = $this->admin_model->get_by_id($id_admin);
        if ($this->input->post('username') == $admin['username']) {
            $username_rules = 'required|trim';
        } else {
            $username_rules = 'required|trim|is_unique[admins.username]';
        }
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', $username_rules);

        if ($this->form_validation->run() == false) {
            $this->admin_edit($id_admin);
        } else {

            if (!empty($_FILES['photo']['name'])) {
                // Hapus foto profil lama
                $old_photo = $admin['photo'];
                if ($old_photo != 'default_photo.jpg') {
                    unlink('./assets/uploads/img/' . $old_photo);
                }

                // Unggah foto profil yang baru
                $photo_path = '';
                $upload_path = './assets/uploads/img/admins/';

                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);
                $photo = 'admins/' . $photo_path;
            } else {
                $photo = $admin['photo'];
            }


            if ($this->input->post('password') == '') {
                $password = $admin['password'];
            } else {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            $data = array(
                'name' => $this->input->post('name'),
                'username' => $this->input->post('username'),
                'photo' => $photo,
                'password' => $password,
            );

            if ($this->admin_model->update($id_admin, $data)) {
                $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
            }
            redirect('dashboard/admin_index');
        }
    }

    public function admin_delete($id_admin)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        // Hapus foto admin jika ada
        $admin = $this->admin_model->get_by_id($id_admin);
        if (!empty($admin['photo'])) {
            $photo_path = './assets/uploads/img/' . $admin['photo'];
            if (file_exists($photo_path)) {
                unlink($photo_path);
            }
        }

        if ($this->admin_model->delete($id_admin)) {
            $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
        } else {
            $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
        }
        redirect('dashboard/admin_index');
    }

    public function subject_index()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $this->load->view('page/dashboard/subject/index', [
            'title' => 'subject',
            'subjects' => $this->subject_model->get_all(),
        ]);
    }

    public function subject_create()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/subject/create', [
            'title' => 'Create subject'
        ]);
    }

    public function subject_store()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('name', 'subject Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->subject_create();
        } else {
            $data = array(
                'name' => $this->input->post('name'),
            );

            if ($this->subject_model->insert($data)) {
                $this->session->set_flashdata('success', 'subject berhasil ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'Gagal menambahkan subject!');
            }
            redirect('dashboard/subject_index');
        }
    }
    public function subject_edit($id_subject)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/subject/edit', [
            'title' => 'Edit subject',
            'subject' => $this->subject_model->get_by_id($id_subject)
        ]);
    }

    public function subject_update($id_subject)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('name', 'subject Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->subject_edit($id_subject);
        } else {
            $data = array(
                'name' => $this->input->post('name'),
            );

            if ($this->subject_model->update($id_subject, $data)) {
                $this->session->set_flashdata('success', 'subject berhasil diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal memperbarui subject!');
            }
            redirect('dashboard/subject_index');
        }
    }

    public function subject_delete($id_subject)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        if ($this->subject_model->delete($id_subject)) {
            $this->session->set_flashdata('success', 'subject berhasil dihapus!');
        } else {
            $this->session->set_flashdata('error', 'Gagal menghapus subject!');
        }
        redirect('dashboard/subject_index');
    }

    public function holiday_index()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $this->load->view('page/dashboard/holiday/index', [
            'title' => 'Holiday',
            'holidays' => $this->holiday_model->get_all(),
            'weekly_holidays' => $this->db->get('weekly_holidays')->result_array()
        ]);
    }

    public function update_weekly_holidays()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        // Menghapus semua data weekly holidays
        $this->db->empty_table('weekly_holidays');

        // Mendapatkan data weekly holidays yang diposting
        $selectedHolidays = $this->input->post('weekly_holidays');

        // Memasukkan data weekly holidays yang diposting ke dalam tabel
        if (!empty($selectedHolidays)) {
            $batchData = [];
            foreach ($selectedHolidays as $holiday) {
                $batchData[] = array('hari' => $holiday);
            }
            $this->db->insert_batch('weekly_holidays', $batchData);
        }

        // Mengarahkan kembali ke halaman indeks holiday
        redirect('dashboard/holiday_index');
    }


    public function holiday_create()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/holiday/create', [
            'title' => 'Create Holiday'
        ]);
    }

    public function holiday_store()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('holiday_name', 'Holiday Name', 'required|trim');
        $this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required|trim|callback_check_date');
        $this->form_validation->set_rules('holiday_type', 'Holiday Type', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->holiday_create();
        } else {
            $data = array(
                'name' => $this->input->post('holiday_name'),
                'date' => $this->input->post('holiday_date'),
                'type' => $this->input->post('holiday_type')
            );

            if ($this->holiday_model->insert($data)) {
                $this->session->set_flashdata('success', 'Holiday berhasil ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'Gagal menambahkan Holiday!');
            }
            redirect('dashboard/holiday_index');
        }
    }

    public function holiday_edit($id_holiday)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/holiday/edit', [
            'title' => 'Edit Holiday',
            'holiday' => $this->holiday_model->get_by_id($id_holiday)
        ]);
    }

    // Fungsi callback untuk memeriksa tanggal
    public function check_date($date)
    {
        $today = date('Y-m-d');
        if ($date <= $today) {
            $this->form_validation->set_message('check_date', 'The Holiday Date must be greater than today\'s date.');
            return false;
        }
        return true;
    }


    public function holiday_update($id_holiday)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('holiday_name', 'Holiday Name', 'required|trim');
        $this->form_validation->set_rules('holiday_date', 'Holiday Date', 'required|trim|callback_check_date');
        $this->form_validation->set_rules('holiday_type', 'Holiday Type', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->holiday_edit($id_holiday);
        } else {
            $data = array(
                'name' => $this->input->post('holiday_name'),
                'date' => $this->input->post('holiday_date'),
                'type' => $this->input->post('holiday_type')
            );

            if ($this->holiday_model->update($id_holiday, $data)) {
                $this->session->set_flashdata('success', 'Holiday berhasil diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal memperbarui Holiday!');
            }
            redirect('dashboard/holiday_index');
        }
    }

    public function holiday_delete($id_holiday)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        if ($this->holiday_model->delete($id_holiday)) {
            $this->session->set_flashdata('success', 'Holiday berhasil dihapus!');
        } else {
            $this->session->set_flashdata('error', 'Gagal menghapus Holiday!');
        }
        redirect('dashboard/holiday_index');
    }


    public function class_index()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $this->load->view('page/dashboard/class/index', [
            'title' => 'class',
            'classes' => $this->class_model->get_all()
        ]);
    }

    public function class_create()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/class/create', [
            'title' => 'class'
        ]);
    }

    public function class_store()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('class_name', 'Class Name', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->class_create();
        } else {

            $data = array(
                'class_name' => $this->input->post('class_name'),
            );
            if ($this->class_model->insert($data)) {
                $this->session->set_flashdata('success', 'User Berhasil Ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'User Berhasil Ditambahkan!');
            }
            redirect('dashboard/class_index');
        }
    }

    public function class_edit($id)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/class/edit', [
            'title' => 'class',
            'class' => $this->class_model->get_by_id($id)
        ]);
    }

    public function class_update($id_class)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $this->form_validation->set_rules('class_name', 'Class Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->class_edit($id_class);
        } else {

            $data = array(
                'class_name' => $this->input->post('class_name'),
            );

            if ($this->class_model->update($id_class, $data)) {
                $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
            }
            redirect('dashboard/class_index');
        }
    }

    public function class_delete($id_class)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        if ($this->class_model->delete($id_class)) {
            $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
        } else {
            $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
        }
        redirect('dashboard/class_index');
    }

    public function student_delete($id_student)
    {
        $student = $this->student_model->get_by_id($id_student);
        if (!empty($student['photo'])) {
            $photo_path = './assets/uploads/img/' . $student['photo'];
            if (file_exists($photo_path)) {
                unlink($photo_path);
            }
        }
        if ($this->student_model->delete($id_student)) {
            $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
        } else {
            $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
        }
        redirect('dashboard/student_index');
    }
    public function teacher_delete($id_teacher)
    {
        $teacher = $this->teacher_model->get_by_id($id_teacher);
        if (!empty($teacher['photo'])) {
            $photo_path = './assets/uploads/img/' . $teacher['photo'];
            if (file_exists($photo_path)) {
                unlink($photo_path);
            }
        }

        if ($this->teacher_model->delete($id_teacher)) {
            $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
        } else {
            $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
        }
        redirect('dashboard/teacher_index');
    }



    public function student_index()
    {
        if ($this->session->userdata('role') == 'admin') {
            $students = $this->student_model->get_all();
        } else if ($this->session->userdata('role') == 'teacher') {
            $teacherId = $_SESSION['id_teacher']; // Mengambil ID guru dari sesi

            $this->db->select('students.*');
            $this->db->from('students');
            $this->db->join('teachers', 'teachers.class_id = students.class_id');
            $this->db->where('teachers.id_teacher', $teacherId);
            $students = $this->db->get()->result_array();
        }

        $this->load->view('page/dashboard/student/index', [
            'title' => 'Student',
            'students' => $students
        ]);
    }
    public function student_create()
    {

        $this->load->view('page/dashboard/student/create', [
            'classes' => $this->class_model->get_all(),
            'title' => 'Student'
        ]);
    }

    public function student_store()
    {
        $this->form_validation->set_rules('class_id', 'Class', 'required');
        $this->form_validation->set_rules('rfid', 'RFID', 'required|trim|is_unique[students.rfid]');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
        $this->form_validation->set_rules('nis', 'NIS', 'required|trim');
        $this->form_validation->set_rules('birth_place', 'Birth Place', 'required|trim');
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required|trim');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric|trim');
        $this->form_validation->set_rules('parent_name', 'Parent Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->student_create();
        } else {
            $upload_path = './assets/uploads/img/students/';

            if (!empty($_FILES['photo']['name'])) {
                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);
            }

            $data = array(
                'class_id' => $this->input->post('class_id'),
                'rfid' => $this->input->post('rfid'),
                'full_name' => $this->input->post('full_name'),
                'nis' => $this->input->post('nis'),
                'photo' => 'students/' . $photo_path,
                'birth_place' => $this->input->post('birth_place'),
                'birth_date' => $this->input->post('birth_date'),
                'gender' => $this->input->post('gender'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_number'),
                'parent_name' => $this->input->post('parent_name'),
            );

            if ($this->student_model->insert($data)) {
                $this->session->set_flashdata('success', 'Student Berhasil Ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'Student Gagal Ditambahkan!');
            }

            redirect('dashboard/student_index');
        }
    }

    public function student_update($id_student)
    {

        $student = $this->student_model->get_by_id($id_student);
        if ($this->input->post('rfid') == $student['rfid']) {
            $rfid_rules = 'required|trim';
        } else {
            $rfid_rules = 'required|trim|is_unique[students.rfid]';
        }
        $this->form_validation->set_rules('class_id', 'Class', 'required');
        $this->form_validation->set_rules('rfid', 'RFID', $rfid_rules);
        $this->form_validation->set_rules('full_name', 'Full Name', 'required|trim');
        $this->form_validation->set_rules('nis', 'NIS', 'required|trim');
        $this->form_validation->set_rules('birth_place', 'Birth Place', 'required|trim');
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required|trim');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric|trim');
        $this->form_validation->set_rules('parent_name', 'Parent Name', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->student_edit($id_student);
        } else {
            if (!empty($_FILES['photo']['name'])) {
                // Hapus foto profil lama
                $old_photo = $student['photo'];
                if ($old_photo != 'default_photo.jpg') {
                    unlink('./assets/uploads/img/' . $old_photo);
                }

                // Unggah foto profil yang baru
                $photo_path = '';
                $upload_path = './assets/uploads/img/students/';

                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);

                $photo = 'students/' . $photo_path;
            } else {
                $photo = $student['photo'];
            }

            $data = array(
                'class_id' => $this->input->post('class_id'),
                'rfid' => $this->input->post('rfid'),
                'full_name' => $this->input->post('full_name'),
                'nis' => $this->input->post('nis'),
                'photo' => $photo,
                'birth_place' => $this->input->post('birth_place'),
                'birth_date' => $this->input->post('birth_date'),
                'gender' => $this->input->post('gender'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_number'),
                'parent_name' => $this->input->post('parent_name'),
            );

            if ($this->student_model->update($id_student, $data)) {
                $this->session->set_flashdata('success', 'Student Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Student Gagal Diperbarui!');
            }

            redirect('dashboard/student_index');
        }
    }


    public function student_edit($id)
    {
        $this->load->view('page/dashboard/student/edit', [
            'title' => 'Student',
            'student' => $this->student_model->get_by_id($id),
            'classes' => $this->class_model->get_all()
        ]);
    }

    public function teacher_index()
    {
        $this->load->view('page/dashboard/teacher/index', [
            'title' => 'Teacher',
            'teachers' => $this->teacher_model->get_all()
        ]);
    }


    public function teacher_create()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/teacher/create', [
            'classes' => $this->class_model->get_all(),
            'title' => 'Teacher'
        ]);
    }

    public function teacher_store()
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('nuptk', 'NUPTK', 'required|trim|numeric');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[teachers.username]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'required');
        $this->form_validation->set_rules('birth_place', 'Birth Place', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('degree', 'Degree', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');
        $this->form_validation->set_rules('class_id', 'Class', 'required');

        if ($this->form_validation->run() == false) {
            $this->teacher_create();
        } else {
            $photo_path = '';
            $upload_path = './assets/uploads/img/teachers/';

            if (!empty($_FILES['photo']['name'])) {
                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);
            }

            $data = array(
                'full_name' => $this->input->post('full_name'),
                'nuptk' => $this->input->post('nuptk'),
                'username' => $this->input->post('username'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'birth_date' => $this->input->post('birth_date'),
                'birth_place' => $this->input->post('birth_place'),
                'gender' => $this->input->post('gender'),
                'degree' => $this->input->post('degree'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_number'),
                'photo' => 'teachers/' . $photo_path,
                'class_id' => $this->input->post('class_id'),
            );

            if ($this->teacher_model->insert($data)) {
                $this->session->set_flashdata('success', 'Teacher Berhasil Ditambahkan!');
            } else {
                $this->session->set_flashdata('error', 'Teacher Gagal Ditambahkan!');
            }

            redirect('dashboard/teacher_index');
        }
    }

    public function teacher_edit($id)
    {
        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }

        $this->load->view('page/dashboard/teacher/edit', [
            'title' => 'Teacher',
            'teacher' => $this->teacher_model->get_by_id($id),
            'classes' => $this->class_model->get_all()
        ]);
    }

    public function teacher_update($id_teacher)
    {

        if ($this->session->userdata('role') !== 'admin') {
            show_404();
        }
        $teacher = $this->teacher_model->get_by_id($id_teacher);
        if ($this->input->post('username') == $teacher['username']) {
            $username_rules = 'required|trim';
        } else {
            $username_rules = 'required|trim|is_unique[teachers.username]';
        }
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'required');

            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        $this->form_validation->set_rules('full_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', $username_rules);
        $this->form_validation->set_rules('nuptk', 'NUPTK', 'required|trim|numeric');
        $this->form_validation->set_rules('birth_date', 'Birth Date', 'required');
        $this->form_validation->set_rules('birth_place', 'Birth Place', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('degree', 'Degree', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|numeric');
        $this->form_validation->set_rules('class_id', 'Class', 'required');

        if ($this->form_validation->run() == false) {
            $this->teacher_edit($id_teacher);
        } else {
            if (!empty($_FILES['photo']['name'])) {
                // Hapus foto profil lama
                $old_photo = $teacher['photo'];
                if ($old_photo != 'default_photo.jpg') {
                    unlink('./assets/uploads/img/' . $old_photo);
                }

                // Unggah foto profil yang baru
                $photo_path = '';
                $upload_path = './assets/uploads/img/teachers/';

                $photo_name = $_FILES['photo']['name'];
                $photo_tmp = $_FILES['photo']['tmp_name'];
                $photo_ext = pathinfo($photo_name, PATHINFO_EXTENSION);
                $photo_path = uniqid() . '.' . $photo_ext;
                move_uploaded_file($photo_tmp, $upload_path . $photo_path);

                $photo = 'teachers/' . $photo_path;
            } else {
                $photo = $teacher['photo'];
            }

            if ($this->input->post('password') == '') {
                $password = $teacher['password'];
            } else {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            }

            $data = array(
                'full_name' => $this->input->post('full_name'),
                'nuptk' => $this->input->post('nuptk'),
                'username' => $this->input->post('username'),
                'photo' => $photo,
                'password' => $password,
                'class_id' => $this->input->post('class_id'),
                'birth_date' => $this->input->post('birth_date'),
                'birth_place' => $this->input->post('birth_place'),
                'gender' => $this->input->post('gender'),
                'degree' => $this->input->post('degree'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_number'),
            );

            if ($this->teacher_model->update($id_teacher, $data)) {
                $this->session->set_flashdata('success', 'User Berhasil Diperbarui!');
            } else {
                $this->session->set_flashdata('error', 'Gagal Memperbarui User!');
            }
            redirect('dashboard/teacher_index');
        }
    }

    public function rekapan_kehadiran()
    {
        if ($this->session->userdata('role') == 'admin') {
            $attendances = $this->db->select('attendances.*, students.full_name, students.rfid')
                ->from('attendances')
                ->join('students', 'students.id_student = attendances.student_id')
                ->order_by('attendances.id_attendance', 'DESC')
                ->get()
                ->result_array();
        } else if ($this->session->userdata('role') == 'teacher') {
            $teacherId = $_SESSION['id_teacher']; // Mengambil ID guru dari sesi

            $this->db->select('attendances.*, students.full_name, students.rfid')
                ->from('attendances')
                ->join('students', 'students.id_student = attendances.student_id')
                ->join('teachers', 'teachers.class_id = students.class_id')
                ->where('teachers.id_teacher', $teacherId)
                ->order_by('attendances.id_attendance', 'DESC');
            $attendances = $this->db->get()->result_array();
        }

        $this->load->view('page/dashboard/rekapan_kehadiran/index', [
            'title' => 'Rekapan Kehadiran',
            'attendances' => $attendances,
            'classes' => $this->db->get('classes')->result_array(),
            'teachers' => $this->db->get('teachers')->result_array(),
            'subjects' => $this->db->get('subjects')->result_array(),
        ]);
    }

    public function attendance_edit($id_attendance)
    {
        $this->load->view('page/dashboard/rekapan_kehadiran/edit', [
            'title' => 'Rekapan Kehadiran',
            'attendance' => $this->db->get_where('attendances', ['id_attendance' => $id_attendance])->row_array()
        ]);
    }
    public function monitoring_waktu()
    {
        // Ambil data waktu saat ini
        $currentDateTime = date('Y-m-d H:i:s');

        // Ambil jumlah siswa yang masuk
        $this->db->from('attendances')
            ->where('date', date('Y-m-d'))
            ->where('masuk', 1);
        $jumlahSiswaMasuk = $this->db->count_all_results();

        $sevenDaysAgo = date('Y-m-d', strtotime('-7 days'));

        if ($this->session->userdata('role') == 'admin') {
            $dataAttendances = $this->db->from('attendances')
                ->where('date >=', $sevenDaysAgo)
                ->get()
                ->result_array();
        } else if ($this->session->userdata('role') == 'teacher') {
            $teacherId = $this->session->userdata('id_teacher');

            $this->db->select('attendances.*');
            $this->db->from('attendances');
            $this->db->join('students', 'students.id_student = attendances.student_id');
            $this->db->join('teachers', 'teachers.class_id = students.class_id');
            $this->db->where('teachers.id_teacher', $teacherId);
            $this->db->where('attendances.date >=', $sevenDaysAgo);
            $dataAttendances = $this->db->get()->result_array();
        }

        $attendanceData = array();
        foreach ($dataAttendances as $attendance) {
            $date = $attendance['date'];
            if (!isset($attendanceData[$date])) {
                $attendanceData[$date] = array(
                    'date' => $date,
                    'masuk' => 0,
                    'keluar' => 0
                );
            }
            if (!empty($attendance['masuk'])) {
                $attendanceData[$date]['masuk']++;
            }

            if (!empty($attendance['keluar'])) {
                $attendanceData[$date]['keluar']++;
            }
        }

        $masukData = array();
        $keluarData = array();
        $categories = array(); // Kategori berdasarkan tanggal
        $counter = 1;

        foreach ($attendanceData as $date => $data) {
            $categories[] = $date; // Tambahkan tanggal ke kategori
            $masukData[] = $data['masuk'];
            $keluarData[] = $data['keluar'];
            $counter++;
        }

        $series = array(
            array(
                'name' => 'Masuk',
                'data' => $masukData
            ),
            array(
                'name' => 'Keluar',
                'data' => $keluarData
            )
        );

        // Ambil jumlah siswa yang keluar
        $this->db->from('attendances')
            ->where('date', date('Y-m-d'))
            ->where('keluar', 1);
        $jumlahSiswaKeluar = $this->db->count_all_results();

        if ($this->session->userdata('role') == 'admin') {
            $totalSiswa = $this->db->get('students')->num_rows();
        } else if ($this->session->userdata('role') == 'teacher') {
            $teacherId = $this->session->userdata('id_teacher');

            $this->db->from('students');
            $this->db->join('teachers', 'teachers.class_id = students.class_id');
            $this->db->where('teachers.id_teacher', $teacherId);
            $totalSiswa = $this->db->count_all_results();
        }

        $operationalTime = $this->db->get_where('operational_times', ['id_operational_time' => 1])->row_array();
        $waktuMasuk = $operationalTime['waktu_masuk'];
        $waktuKeluar = $operationalTime['waktu_keluar'];
        $telat = $operationalTime['telat'];

        $this->load->view('page/dashboard/monitoring_waktu/index', [
            'title' => 'Monitoring Waktu',
            'currentDateTime' => $currentDateTime,
            'jumlahSiswaMasuk' => $jumlahSiswaMasuk,
            'jumlahSiswaKeluar' => $jumlahSiswaKeluar,
            'totalSiswa' => $totalSiswa,
            'operationalTime' => $operationalTime,
            'waktuMasuk' => $waktuMasuk,
            'waktuKeluar' => $waktuKeluar,
            'telat' => $telat,
            'series' => json_encode($series),
            'categories' => json_encode($categories) // Mengubah kategori menjadi JSON
        ]);
    }



    public function setting()
    {
        $operationalTime = $this->db->get_where('operational_times', ['id_operational_time' => 1])->row_array();

        $this->load->view('page/dashboard/setting', [
            'title' => 'Setting',
            'operationalTime' => $operationalTime,
            'secret_key' => $this->db->get_where('settings', ['name' => 'secret_key'])->row_array(),
            'weekend_attendance' => $this->db->get_where('settings', ['name' => 'weekend_attendance'])->row_array(),
            'home_background' => $this->db->get_where('settings', ['name' => 'home_background'])->row_array(),
        ]);
    }

    public function setting2_update()
    {
        // Mengambil data dari form
        $waktu_masuk_awal = $this->input->post('waktu_masuk_awal');
        $waktu_masuk_akhir = $this->input->post('waktu_masuk_akhir');
        $waktu_keluar_awal = $this->input->post('waktu_keluar_awal');
        $waktu_keluar_akhir = $this->input->post('waktu_keluar_akhir');
        $telat = $this->input->post('telat');
        $secret_key = $this->input->post('secret_key');
        $weekend_attendance = $this->input->post('weekend_attendance');

        // Update data di database
        $operationalTime = $this->db->get_where('operational_times', ['id_operational_time' => 1])->row_array();
        $operationalTime['waktu_masuk'] = $waktu_masuk_awal . '-' . $waktu_masuk_akhir;
        $operationalTime['waktu_keluar'] = $waktu_keluar_awal . '-' . $waktu_keluar_akhir;
        $operationalTime['telat'] = $telat;
        $this->db->update('operational_times', $operationalTime, ['id_operational_time' => 1]);

        $this->db->update('settings', ['value' => $secret_key], ['name' => 'secret_key']);
        $this->db->update('settings', ['value' => $weekend_attendance], ['name' => 'weekend_attendance']);

        // Redirect atau tampilkan pesan sukses
        redirect('dashboard/setting');
    }


    public function setting_update()
    {
        // Mengambil data dari form
        $waktu_masuk_awal = $this->input->post('waktu_masuk_awal');
        $waktu_masuk_akhir = $this->input->post('waktu_masuk_akhir');
        $waktu_keluar_awal = $this->input->post('waktu_keluar_awal');
        $waktu_keluar_akhir = $this->input->post('waktu_keluar_akhir');
        $telat = $this->input->post('telat');
        $secret_key = $this->input->post('secret_key');
        $weekend_attendance = $this->input->post('weekend_attendance');

        // Update data di database
        $operationalTime = $this->db->get_where('operational_times', ['id_operational_time' => 1])->row_array();
        $operationalTime['waktu_masuk'] = $waktu_masuk_awal . '-' . $waktu_masuk_akhir;
        $operationalTime['waktu_keluar'] = $waktu_keluar_awal . '-' . $waktu_keluar_akhir;
        $operationalTime['telat'] = $telat;
        $this->db->update('operational_times', $operationalTime, ['id_operational_time' => 1]);

        $this->db->update('settings', ['value' => $secret_key], ['name' => 'secret_key']);
        $this->db->update('settings', ['value' => $weekend_attendance], ['name' => 'weekend_attendance']);

        // Upload home background
        if ($_FILES['home_background']['name']) {
            $config['upload_path'] = './assets/uploads/img/settings/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size'] = 2048; // 2MB
            $config['file_name'] = uniqid() . '_' . str_replace(' ', '_', $_FILES['home_background']['name']);

            $this->load->library('upload', $config);

            $homeBackground = $this->db->get_where('settings', ['name' => 'home_background'])->row_array();
            unlink('./assets/uploads/img/' . $homeBackground['value']);
            if ($this->upload->do_upload('home_background')) {
                $photo = 'settings/' . $config['file_name'];
                $this->db->update('settings', ['value' => $photo], ['name' => 'home_background']);
            } else {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', '<div class="alert alert-danger">' . $error . '</div>');
            }
        }

        // Redirect atau tampilkan pesan sukses
        redirect('dashboard/setting');
    }



    public function attendance_update($attendanceId)
    {
        // Jika validasi berhasil, lakukan update pada tabel attendances
        $data = [
            'masuk' => $this->input->post('masuk'),
            'waktu_masuk' => $this->input->post('waktu_masuk'),
            'keluar' => $this->input->post('keluar'),
            'waktu_keluar' => $this->input->post('waktu_keluar')
        ];

        $this->db->where('id_attendance', $attendanceId);
        $this->db->update('attendances', $data);

        // Set flashdata untuk pesan sukses
        $this->session->set_flashdata('message', '<div class="alert alert-success">Attendance has been updated.</div>');

        redirect('dashboard/rekapan_kehadiran');
    }


    public function scan_rfid()
    {
        if ($this->session->userdata('role') == 'admin') {
            $students = $this->student_model->get_all();
        } else if ($this->session->userdata('role') == 'teacher') {
            $teacherId = $_SESSION['id_teacher']; // Mengambil ID guru dari sesi

            $this->db->select('students.*');
            $this->db->from('students');
            $this->db->join('teachers', 'teachers.class_id = students.class_id');
            $this->db->where('teachers.id_teacher', $teacherId);
            $students = $this->db->get()->result_array();
        }
        $this->load->view('page/dashboard/scan_rfid/index', [
            'title' => 'Scan RFID',
            'students' => $students
        ]);
    }

    public function scan_rfid_update($studentId)
    {
        // Mendapatkan nilai jenis tindakan (masuk atau keluar) dari form
        $type = $this->input->post('type');

        // Menyiapkan data kehadiran berdasarkan jenis tindakan
        $attendanceData = array(
            'student_id' => $studentId,
            'date' => date('Y-m-d')
        );

        if ($type == 'masuk') {
            $attendanceData['masuk'] = 1;
            $attendanceData['waktu_masuk'] = date('H:i:s');
        } elseif ($type == 'keluar') {
            $attendanceData['keluar'] = 1;
            $attendanceData['waktu_keluar'] = date('H:i:s');
        }

        // Memeriksa apakah data kehadiran sudah ada untuk siswa pada hari ini
        $this->db->where('student_id', $studentId)
            ->where('DATE(date)', date('Y-m-d'));
        $query = $this->db->get('attendances');

        if ($query->num_rows() > 0) {
            // Data kehadiran sudah ada, lakukan pembaruan (update) data
            $this->db->where('student_id', $studentId)
                ->where('DATE(date)', date('Y-m-d'))
                ->update('attendances', $attendanceData);
        } else {
            // Data kehadiran belum ada, lakukan pembuatan (insert) data baru
            $this->db->insert('attendances', $attendanceData);
        }

        // Redirect atau melakukan tindakan lain setelah pembaruan atau pembuatan data kehadiran
        redirect('dashboard/scan_rfid'); // Ganti 'dashboard' dengan halaman tujuan yang sesuai
    }

    public function generate_pdf()
    {
        // Mengambil data yang dikirim dari form
        $teacher_id = $this->input->post('teacher');
        $class_id = $this->input->post('class');
        $subject_id = $this->input->post('subject');
        $tanggal = $this->input->post('tanggal');

        // Proses pengambilan data guru, kelas, dan mapel berdasarkan ID yang dipilih
        $teacher = $this->db->get_where('teachers', ['id_teacher' => $teacher_id])->row_array();
        $class = $this->db->get_where('classes', ['id_class' => $class_id])->row_array();
        $subject = $this->db->get_where('subjects', ['id_subject' => $subject_id])->row_array();

        // Proses pengambilan data kehadiran siswa berdasarkan kelas yang dipilih dan tanggal (jika ada)
        $this->db->select('attendances.*, students.full_name, students.rfid')
            ->from('attendances')
            ->join('students', 'students.id_student = attendances.student_id')
            ->join('classes', 'classes.id_class = students.class_id')
            ->where('classes.id_class', $class_id);

        if (!empty($tanggal)) {
            $this->db->where('attendances.date', $tanggal);
        }

        $attendances = $this->db->order_by('attendances.id_attendance', 'DESC')
            ->get()
            ->result_array();

        // Proses generasi PDF menggunakan library MPDF
        $mpdf = new \Mpdf\Mpdf();

        // CSS untuk memperbagus tampilan PDF
        $css = file_get_contents('path/to/your/css/file.css');
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);

        // Konten PDF
        $html = '<h1>Rekap Kehadiran</h1>';
        $html .= '<p>Kelas: ' . $class['class_name'] . '</p>';
        $html .= '<p>Guru: ' . $teacher['full_name'] . '</p>';
        $html .= '<p>Mata Pelajaran: ' . $subject['name'] . '</p>';

        // Tabel data kehadiran siswa
        $html .= '<table style="border-collapse: collapse; width: 100%;">';
        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">#</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Nama</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Foto</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Masuk</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Waktu Masuk</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Keluar</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Waktu Keluar</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Status</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Ket</th>';
        $html .= '<th style="border: 1px solid black; padding: 8px;">Tanggal</th>';
        $html .= '</tr>';
        $html .= '</thead>';
        $html .= '<tbody>';

        $no = 1;
        foreach ($attendances as $attendance) {
            $html .= '<tr>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $no++ . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['full_name'] . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;"><img src="' . $attendance['photo'] . '" width="30" alt=""></td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . ($attendance['masuk'] ? 'Ya' : 'Tidak') . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['waktu_masuk'] . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . ($attendance['keluar'] ? 'Ya' : 'Tidak') . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['waktu_keluar'] . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['status_hadir'] . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['ket'] . '</td>';
            $html .= '<td style="border: 1px solid black; padding: 8px;">' . $attendance['date'] . '</td>';
            $html .= '</tr>';
        }

        $html .= '</tbody>';
        $html .= '</table>';

        // Menulis konten ke dalam PDF
        $mpdf->WriteHTML($html);

        // Menyimpan PDF ke file atau mengirimkan langsung sebagai respons
        $mpdf->Output('rekap_kehadiran.pdf', 'D');
    }
}
