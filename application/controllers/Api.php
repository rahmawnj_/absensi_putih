<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(['url']);
    }

    public function updateabsen_post()
    {
        $idAttendance = $this->input->post('id_attendance');
        $photo = $this->input->post('photo');
        $attendance = $this->db->select('*')
            ->from('attendances')
            ->join('students', 'students.id_student = attendances.student_id')
            ->where('attendances.id_attendance', $idAttendance)
            ->get()
            ->row_array();

        $this->db->update('attendances', ['photo' => $photo], ['id_attendance' => $idAttendance]);
        $this->response(['status' => 'success', 'nama' => $attendance['full_name']]);
    }

    public function absensijson_post()
    {
        $rfid = $this->input->post('rfid');
        $key = $this->input->post('key');

        if (is_null($key)) {
            $this->response(['ket' => 'key perlu dikirim', 'status' => 'error']);
        }

        if (is_null($rfid)) {
            $this->response(['ket' => 'rfid perlu dikirim', 'status' => 'error']);
        }

        $settingSecretKey = $this->db->get_where('settings', ['name' => 'secret_key'])->row_array();
        if ($settingSecretKey['value'] != $key) {
            $this->response(['ket' => 'key salah', 'status' => 'error']);
        }

        $student = $this->db->get_where('students', ['rfid' => $rfid])->row_array();

        if (!$student) {
            $this->response(['ket' => 'rfid tidak ditemukan', 'status' => 'error']);
        }

        $operationalTime = $this->db->get_where('operational_times', ['id_operational_time' => 1])->row_array();

        $masuk = explode('-', $operationalTime['waktu_masuk']);
        $keluar = explode('-', $operationalTime['waktu_keluar']);
        $masukAwal = $masuk[0];
        $masukAkhir = $masuk[1];
        $keluarAwal = $keluar[0];
        $keluarAkhir = $keluar[1];
        $telat = $operationalTime['telat'];

        $dateNow = date('Y-m-d');
        $dayNow = date('l');
        $timeNow = date("H:i");

        // Menjalankan query untuk mencari hari libur
        $query = $this->db->get_where('weekly_holidays', ['hari' => $dayNow]);

        // Memeriksa apakah hari ini adalah hari libur
        if ($query->num_rows() > 0) {
            // Hari ini adalah hari libur
            $this->response(['ket' => 'Hari ini adalah hari libur', 'status' => 'success']);
        }

        // Menjalankan query untuk mencari libur nasional berdasarkan tanggal saat ini
        $holidayQuery = $this->db->get_where('holidays', ['date' => $dateNow]);

        // Memeriksa apakah hari ini adalah libur nasional
        if ($holidayQuery->num_rows() > 0) {
            // Hari ini adalah libur nasional
            $this->response(['ket' => 'Hari ini adalah libur nasional', 'status' => 'success']);
        }

        $attandance = $this->db->get_where('attendances', ['date' => $dateNow, 'student_id' => $student['id_student']])->row_array();

        if ($timeNow < $masukAkhir && $timeNow > $masukAwal) {
            if (!$attandance) {
                if ($timeNow < $telat) {
                    $ket = 'Hadir - Tepat waktu';
                } else {
                    $ket = 'Hadir - Telat';
                }

                $this->db->insert('attendances', [
                    'student_id' => $student['id_student'],
                    'masuk' => 1,
                    'waktu_masuk' => $timeNow,
                    'keluar' => 0,
                    'status_hadir' => 'Hadir',
                    'ket' => $ket,
                    'photo' => $this->input->post('photo'),
                    'date' => $dateNow,
                ]);
                $newAttendance = $this->db->get_where('attendances', ['student_id' => $student['id_student'], 'date' => $dateNow])->row_array();
            } else {
                $this->response(['ket' => 'Sudah Absensi', 'status' => 'error']);
            }
        } elseif ($timeNow < $keluarAkhir && $timeNow > $keluarAwal) {
            if ($attandance) {
                $this->db->where(['date' => $dateNow, 'student_id' => $student['id_student']])
                    ->update('attendances', [
                        'keluar' => 1,
                        'waktu_keluar' => $timeNow,
                        'photo' => $this->input->post('photo'),
                    ]);
                $ket = 'pulang';
                $newAttendance = $this->db->get_where('attendances', ['student_id' => $student['id_student'], 'date' => $dateNow])->row_array();
            } else {
                $this->response(['ket' => 'Anda belum melakukan absensi masuk', 'status' => 'error']);
            }
        } else {
            $this->response(['ket' => 'Error Waktu Operasional', 'status' => 'error']);
        }

        $this->response(['ket' => $ket, 'status' => 'success', 'data' => ['nama' => $student['full_name'], 'nis' => $student['nis']], 'id_attendance' => $newAttendance['id_attendance'], 'time' => date('H:i:s d/m/Y')]);
    }
}
