<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('welcome_message', [
			'home_background' => $this->db->get_where('settings', ['name' => 'home_background'])->row_array(),
		]);
	}
	public function rekapan_kehadiran()
	{
		$this->load->view('page/landingpage/rekapan_kehadiran', [
			'attendances' => $this->db->select('attendances.*, students.full_name, students.rfid')
				->from('attendances')
				->join('students', 'students.id_student = attendances.student_id')
				->order_by('attendances.id_attendance', 'DESC')
				->get()
				->result_array(),

		]);
	}

	public function cron()
	{
		// Mengambil semua siswa
		$students = $this->db->get('students')->result_array();

		// Mendapatkan tanggal hari ini
		$dateNow = date('Y-m-d');

		foreach ($students as $student) {
			$attendance = $this->db->get_where('attendances', ['student_id' => $student['id_student'], 'date' => $dateNow])->row_array();

			// Jika tidak ada catatan kehadiran, buatkan catatan dengan status "alfa"
			if (!$attendance) {
				$this->db->insert('attendances', [
					'student_id' => $student['id_student'],
					'masuk' => 0,
					'keluar' => 0,
					'status_hadir' => 'Alfa',
					'ket' => 'Tidak Hadir',
					'photo' => null,
					'date' => $dateNow,
				]);

				echo "Catatan kehadiran alfa dibuat untuk siswa dengan ID: " . $student['id_student'] . "\n";
			}
		}

		echo "Proses pembuatan catatan kehadiran alfa selesai.\n";
	}

	public function task()
	{
		$this->load->view('page/landingpage/task', [
		]);
	}
}
