<?php

class class_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('classes', $data);
    }

    public function get_all()
    {
        return $this->db->get('classes')->result_array();
    }

    public function get_by_id($id_class)
    {
        return $this->db->get_where('classes', ['id_class' => $id_class])->row_array();
    }

    public function update($id_class, $data)
    {
        return $this->db->update('classes', $data, ['id_class' => $id_class]);
    }

    public function delete($id_class)
    {
        return $this->db->where('id_class', $id_class)->delete('classes');
    }
}
