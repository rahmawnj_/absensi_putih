<?php

class teacher_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('teachers', $data);
    }

    public function get_all()
    {
        return $this->db->get('teachers')->result_array();
    }

    public function get_by_id($id_teacher)
    {
        return $this->db->get_where('teachers', ['id_teacher' => $id_teacher])->row_array();
    }

    public function update($id_teacher, $data)
    {
        return $this->db->update('teachers', $data, ['id_teacher' => $id_teacher]);
    }

    public function delete($id_teacher)
    {
        return $this->db->where('id_teacher', $id_teacher)->delete('teachers');
    }
}
