<?php

class subject_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('subjects', $data);
    }

    public function get_all()
    {
        return $this->db->get('subjects')->result_array();
    }

    public function get_by_id($id_subject)
    {
        return $this->db->get_where('subjects', ['id_subject' => $id_subject])->row_array();
    }

    public function update($id_subject, $data)
    {
        return $this->db->update('subjects', $data, ['id_subject' => $id_subject]);
    }

    public function delete($id_subject)
    {
        return $this->db->where('id_subject', $id_subject)->delete('subjects');
    }
}
