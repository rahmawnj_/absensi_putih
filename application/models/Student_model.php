<?php

class student_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('students', $data);
    }

    public function get_all()
    {
        return $this->db->get('students')->result_array();
    }

    public function get_by_id($id_student)
    {
        return $this->db->get_where('students', ['id_student' => $id_student])->row_array();
    }

    public function update($id_student, $data)
    {
        return $this->db->update('students', $data, ['id_student' => $id_student]);
    }

    public function delete($id_student)
    {
        return $this->db->where('id_student', $id_student)->delete('students');
    }
}
