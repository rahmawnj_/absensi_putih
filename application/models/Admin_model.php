<?php

class Admin_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('admins', $data);
    }

    public function get_all()
    {
        return $this->db->get('admins')->result_array();
    }

    public function get_by_id($id_admin)
    {
        return $this->db->get_where('admins', ['id_admin' => $id_admin])->row_array();
    }

    public function update($id_admin, $data)
    {
        return $this->db->update('admins', $data, ['id_admin' => $id_admin]);
    }

    public function delete($id_admin)
    {
        return $this->db->where('id_admin', $id_admin)->delete('admins');
    }
}
