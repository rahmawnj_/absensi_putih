<?php

class holiday_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert($data)
    {
        return $this->db->insert('holidays', $data);
    }

    public function get_all()
    {
        return $this->db->get('holidays')->result_array();
    }

    public function get_by_id($id_holiday)
    {
        return $this->db->get_where('holidays', ['id_holiday' => $id_holiday])->row_array();
    }

    public function update($id_holiday, $data)
    {
        return $this->db->update('holidays', $data, ['id_holiday' => $id_holiday]);
    }

    public function delete($id_holiday)
    {
        return $this->db->where('id_holiday', $id_holiday)->delete('holidays');
    }
}
