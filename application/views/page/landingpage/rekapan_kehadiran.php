<?php $this->load->view('layouts/landingpage/head') ?>
<div id="header" class="app-header p-3" style="background-color: white;">
    <h1>Rekapan Kehadiran</h1>
</div>
<main class="container">
    <div class="card mt-5">
        <div class="card-body">
            <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th>Nama</th>
                        <th>RFID</th>
                        <th>Masuk</th>
                        <th>Waktu Masuk</th>
                        <th>Keluar</th>
                        <th>Waktu Keluar</th>
                        <th>Foto</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0;
                    foreach ($attendances as $attendance) : ?>
                        <tr>
                            <td><?= ++$no; ?></td>
                            <td><?= $attendance['full_name'] ?></td>
                            <td><?= $attendance['rfid'] ?></td>
                            <td><?= $attendance['masuk'] ? 'Ya' : 'Tidak' ?></td>
                            <td><?= $attendance['waktu_masuk'] ? 'Ya' : 'Tidak' ?></td>
                            <td><?= $attendance['keluar'] ?></td>
                            <td><?= $attendance['waktu_keluar'] ?></td>
                            <td><img src="<?= $attendance['photo'] ?>" width="30" alt=""></td>
                            <td>
                                <a class="btn-sm btn bg-warning text-white" href="<?= base_url('dashboard/attendance_edit/' . $attendance['id_attendance']) ?>"><i class="fa fa-edit"></i></a>
                                <a class="btn-sm btn bg-black text-white" href="<?= base_url('dashboard/print_edit/' . $attendance['id_attendance']) ?>"><i class="fa fa-print"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php $this->load->view('layouts/landingpage/foot') ?>