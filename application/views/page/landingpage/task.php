<!DOCTYPE html>
<html>

<head>
    <title>Monitoring Cron Job</title>
</head>

<body>
    <h1>Monitoring Cron Job</h1>
    <div id="status"></div>

    <script>
        function checkTime() {
            var currentDate = new Date();
            var currentHour = currentDate.getHours();
            var currentMinute = currentDate.getMinutes();

            // Cek apakah sudah lewat pukul 8.30
            if (currentHour > 8 || (currentHour === 8 && currentMinute >= 30)) {
                executeCron();
                clearInterval(checkInterval);
            }
        }

        function executeCron() {
            var statusElement = document.getElementById('status');
            statusElement.innerHTML = 'Menjalankan cron job...';

            // Menggunakan AJAX untuk mengakses API welcome/cron
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '<?= base_url('welcome/cron') ?>', true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        statusElement.innerHTML = 'Cron job berhasil dijalankan pada pukul 8.30';
                    } else {
                        statusElement.innerHTML = 'Gagal menjalankan cron job';
                    }
                }
            };
            xhr.send();
        }

        // Set interval pengecekan setiap detik
        var checkInterval = setInterval(checkTime, 1000);
    </script>
</body>

</html>