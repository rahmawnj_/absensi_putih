<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">

                <?= form_open_multipart('dashboard/student_store'); ?>
                <div class="img-preview d-flex">
                    <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                </div>
                <?= $this->session->flashdata('message'); ?>
                <fieldset>
                    <?php if ($this->session->userdata('role') == 'admin') : ?>
                        <div class="form-group">
                            <label for="class_id">Class</label>
                            <select class="form-control" name="class_id" id="class_id" required>
                                <option value="">Select Class</option>
                                <?php foreach ($classes as $class) : ?>
                                    <option value="<?= $class['id_class']; ?>" <?= set_select('class_id', $class['id_class']); ?>><?= $class['class_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('class_id', '<span class="text-danger">', '</span>'); ?>
                        </div>
                    <?php else : ?>
                        <input type="hidden" name="class_id" value="<?= $this->db->select('*')->from('teachers')->where('id_teacher', $this->session->userdata('id_teacher'))->get()->row_array()['class_id']; ?>">
                    <?php endif ?>

                    <div class="form-group">
                        <label for="rfid">RFID</label>
                        <input type="text" class="form-control" name="rfid" id="rfid" placeholder="Enter RFID" value="<?= set_value('rfid'); ?>" required>
                        <?= form_error('rfid', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                        <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Enter Full Name" value="<?= set_value('full_name'); ?>" required>
                        <?= form_error('full_name', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="nis">NIS</label>
                        <input type="text" class="form-control" name="nis" id="nis" placeholder="Enter NIS" value="<?= set_value('nis'); ?>" required>
                        <?= form_error('nis', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <input type="file" class="form-control" name="photo" id="photo" required>
                        <?= form_error('photo', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="birth_place">Birth Place</label>
                        <input type="text" class="form-control" name="birth_place" id="birth_place" placeholder="Enter Birth Place" value="<?= set_value('birth_place'); ?>" required>
                        <?= form_error('birth_place', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="birth_date">Birth Date</label>
                        <input type="date" class="form-control" name="birth_date" id="birth_date" required>
                        <?= form_error('birth_date', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select class="form-control" name="gender" id="gender" required>
                            <option value="">Select Gender</option>
                            <option value="Male" <?= set_select('gender', 'Male'); ?>>Male</option>
                            <option value="Female" <?= set_select('gender', 'Female'); ?>>Female</option>
                        </select>
                        <?= form_error('gender', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" name="address" id="address" placeholder="Enter Address" rows="3" required><?= set_value('address'); ?></textarea>
                        <?= form_error('address', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Phone Number</label>
                        <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Enter Phone Number" value="<?= set_value('phone_number'); ?>" required>
                        <?= form_error('phone_number', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <label for="parent_name">Parent Name</label>
                        <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Enter Parent Name" value="<?= set_value('parent_name'); ?>" required>
                        <?= form_error('parent_name', '<span class="text-danger">', '</span>'); ?>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                    </div>
                </fieldset>

                <?= form_close() ?>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>