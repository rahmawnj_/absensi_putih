<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                    <a href="<?= base_url('/dashboard/student_create') ?>" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a>

                </div>
            </div>
            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

            <div class="panel-body" class="table-responsive">
                <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Photo</th>
                            <th>Nama</th>
                            <th>RFID</th>
                            <th>NIS</th>
                            <th>TTL</th>
                            <th>JK</th>
                            <th>Alamat</th>
                            <th>No. HP</th>
                            <th>Nama Orang Tua</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($students as $student) : ?>
                            <tr>
                                <td><?= ++$no; ?></td>
                                <td>
                                    <img src="<?= base_url('/assets/uploads/img/' . $student['photo']) ?>" class="img-fluid img-thumbnail mx-auto d-block text-center overflow-hidden" style="height:100px; width:100px" alt="">
                                </td>
                                <td><?= $student['full_name'] ?></td>
                                <td><?= $student['rfid'] ?></td>
                                <td><?= $student['nis'] ?></td>
                                <td><?= $student['birth_place'] ?>, <?= $student['birth_date'] ?></td>
                                <td><?= $student['gender'] ?></td>
                                <td><?= $student['address'] ?></td>
                                <td><?= $student['phone_number'] ?></td>
                                <td><?= $student['parent_name'] ?></td>
                                <td>
                                    <a class="btn-sm btn bg-warning text-white" href="<?= base_url('dashboard/student_edit/' . $student['id_student']) ?>"><i class="fa fa-edit"></i></a>
                                    <a id="delete-button" class="btn-sm btn bg-danger text-white" href="<?= base_url('dashboard/student_delete/' . $student['id_student']) ?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>

                </table>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>