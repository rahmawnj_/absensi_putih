<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                    <a href="<?= base_url('/dashboard/holiday_create') ?>" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            
            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

            <div class="panel-body table-responsive">
                <div class="alert alert-success">
                    <form action="<?= base_url('dashboard/update_weekly_holidays') ?>" method="post">
                        <?= $this->session->flashdata('message'); ?>
                        <fieldset>
                            <legend>Libur Mingguan</legend>
                            <div class="form-group">
                                <div class="row">
                                    <?php
                                    $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

                                    foreach ($days as $day) :
                                        $checked = in_array($day, array_column($weekly_holidays, 'hari')) ? 'checked' : '';
                                    ?>
                                        <div class="col-6 col-md-3">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="weekly_holidays[]" value="<?= $day ?>" id="checkbox<?= $day ?>" <?= $checked ?>>
                                                <label class="form-check-label" for="checkbox<?= $day ?>">
                                                    <?php
                                                    switch ($day) {
                                                        case 'Sunday':
                                                            echo 'Minggu';
                                                            break;
                                                        case 'Monday':
                                                            echo 'Senin';
                                                            break;
                                                        case 'Tuesday':
                                                            echo 'Selasa';
                                                            break;
                                                        case 'Wednesday':
                                                            echo 'Rabu';
                                                            break;
                                                        case 'Thursday':
                                                            echo 'Kamis';
                                                            break;
                                                        case 'Friday':
                                                            echo 'Jumat';
                                                            break;
                                                        case 'Saturday':
                                                            echo 'Sabtu';
                                                            break;
                                                        default:
                                                            echo $day;
                                                    }
                                                    ?>
                                                </label>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </fieldset>
                    </form>
                </div>



                <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($holidays as $holiday) : ?>
                            <tr>
                                <td><?= ++$no; ?></td>

                                <td><?= $holiday['name'] ?></td>
                                <td><?= $holiday['date'] ?> <?= date('Y-m-d') ?></td>
                                <td>
                                    <?php
                                    if ($holiday['date'] > date('Y-m-d')) {
                                    ?>
                                        <a class="btn-sm btn bg-warning text-white" href="<?= base_url('dashboard/holiday_edit/' . $holiday['id_holiday']) ?>"><i class="fa fa-edit"></i></a>
                                        <a id="delete-button" class="btn-sm btn bg-danger text-white" href="<?= base_url('dashboard/holiday_delete/' . $holiday['id_holiday']) ?>"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>