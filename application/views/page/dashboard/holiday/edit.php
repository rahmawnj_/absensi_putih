<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>

                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">
                <form action="<?= base_url('dashboard/holiday_update/' . $holiday['id_holiday']) ?>" method="post">
                    <?= $this->session->flashdata('message'); ?>
                    <fieldset>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="holiday_name">Holiday Name</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('holiday_name', $holiday['name']) ?>" name="holiday_name" id="holiday_name" aria-describedby="holiday_name" placeholder="Enter Holiday Name" required>
                                <span class="text-danger">
                                    <?= form_error('holiday_name') ?>
                                </span>
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="form-group">
                                <label for="holiday_date">Holiday Date</label>
                                <input autocomplete="off" type="date" class="form-control" value="<?= set_value('holiday_date') ?>" name="holiday_date" id="holiday_date" aria-describedby="holiday_date" required>
                                <span class="text-danger">
                                    <?= form_error('holiday_date') ?>
                                </span>
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="form-group">
                                <label for="holiday_type">Holiday Type</label>
                                <select class="form-control" name="holiday_type" id="holiday_type" required>
                                    <option value="">Select Type</option>
                                    <option value="National" <?= set_select('holiday_type', 'National', $holiday['type'] == 'National') ?>>National</option>
                                    <option value="Religious" <?= set_select('holiday_type', 'Religious', $holiday['type'] == 'Religious') ?>>Religious</option>
                                    <option value="Local" <?= set_select('holiday_type', 'Local', $holiday['type'] == 'Local') ?>>Local</option>
                                </select>
                                <span class="text-danger">
                                    <?= form_error('holiday_type') ?>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                        </div>
                    </fieldset>
                </form>


            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>