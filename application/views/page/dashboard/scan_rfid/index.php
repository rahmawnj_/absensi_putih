<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                    <a href="<?= base_url('/dashboard/student_create') ?>" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

            <div class="panel-body table-responsive">
                <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Nama</th>
                            <th>RFID</th>
                            <th>Masuk</th>
                            <th>Waktu Masuk</th>
                            <th>Keluar</th>
                            <th>Waktu Keluar</th>
                            <th>Foto</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($students as $student) : ?>
                            <?php  // Ganti dengan ID siswa yang sesuai

                            $this->db->select('*')->from('attendances')->where('student_id', $student['id_student'])
                                ->where('DATE(date)', date('Y-m-d'));
                            $attendance = $this->db->get()->row_array();

                            ?>
                            <tr>
                                <td><?= ++$no; ?></td>
                                <td><?= $student['full_name'] ?></td>
                                <td><?= $student['rfid'] ?></td>
                                <td><?= isset($attendance['masuk']) ? 'Ya' : 'Tidak' ?></td>
                                <td><?= $attendance['waktu_masuk'] ?? null ?></td>
                                <td><?= isset($attendance['keluar']) ? 'Ya' : 'Tidak' ?></td>
                                <td><?= $attendance['waktu_keluar'] ?? null ?></td>
                                <td><?= $attendance['photo'] ?? null ?></td>
                                <td>
                                    <div style="display: flex; gap: 10px;">
                                        <?php if (isset($attendance['masuk']) != 1) :  ?>
                                            <form action="<?= base_url('dashboard/scan_rfid_update/' . $student['id_student']) ?>" method="post">
                                                <input type="hidden" name="type" value="masuk">
                                                <button class="btn btn-primary">Masuk</button>
                                            </form>
                                        <?php endif ?>
                                        <?php if (isset($attendance['keluar']) != 1) :  ?>
                                            <form action="<?= base_url('dashboard/scan_rfid_update/' . $student['id_student']) ?>" method="post">
                                                <input type="hidden" name="type" value="keluar">
                                                <button class="btn btn-danger">Keluar</button>
                                            </form>
                                        <?php endif ?>
                                    </div>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>