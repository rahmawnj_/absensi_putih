<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

    <div id="content" class="app-content">
        <?= form_open_multipart('dashboard/setting_update/'); ?>

        <div class="row g-3">
            <div class="col-sm">
                <label for="waktu_masuk_awal">Waktu Masuk</label>
                <div class="input-group mb-3">
                    <input type="time" class="form-control" name="waktu_masuk_awal" value="<?= set_value('waktu_masuk_awal', explode('-', $operationalTime['waktu_masuk'])[0]) ?>" required>
                    <span class="input-group-text">-</span>
                    <input type="time" class="form-control" name="waktu_masuk_akhir" value="<?= set_value('waktu_masuk_akhir', explode('-', $operationalTime['waktu_masuk'])[1]) ?>" required>
                </div>
                <span class="text-danger"><?= form_error('waktu_masuk_awal') ?></span>
            </div>
            <div class="col-sm">
                <label for="waktu_keluar_awal">Waktu Keluar</label>
                <div class="input-group mb-3">
                    <input type="time" class="form-control" name="waktu_keluar_awal" value="<?= set_value('waktu_keluar_awal', explode('-', $operationalTime['waktu_keluar'])[0]) ?>" required>
                    <span class="input-group-text">-</span>
                    <input type="time" class="form-control" name="waktu_keluar_akhir" value="<?= set_value('waktu_keluar_akhir', explode('-', $operationalTime['waktu_keluar'])[1]) ?>" required>
                </div>
                <span class="text-danger"><?= form_error('waktu_keluar_awal') ?></span>
            </div>
            <div class="col-sm">
                <label for="telat">Telat</label>
                <input type="time" class="form-control" name="telat" value="<?= set_value('telat', $operationalTime['telat']) ?>" required>
                <span class="text-danger"><?= form_error('telat') ?></span>
            </div>
        </div>
        <div class="mb-3">
            <div class="form-group">
                <label for="secret_key">Secret Key</label>
                <input type="text" class="form-control" name="secret_key" placeholder="Masukkan teks" value="<?= set_value('secret_key', $secret_key['value']) ?>" required>
                <span class="text-danger"><?= form_error('secret_key') ?></span>
            </div>
        </div>
        <div class="mb-3">
            <div class="form-group">
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="weekend_attendance" name="weekend_attendance" <?= $weekend_attendance['value'] == 'on' ? 'checked' : '' ?>>
                    <label class="form-check-label" for="weekend_attendance">Weekend Attendance</label>
                </div>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <div class="text-center">
            <img src="<?= base_url('/assets/uploads/img/' . $home_background['value']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
        </div>
        <div class="mb-3">
            <div class="form-group">
                <label for="home_background">Home Background</label>
                <input type="file" class="form-control" name="home_background" value="">
            </div>
        </div>

        <div class="form-group text-center mb-3">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?= form_close() ?>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>