<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">

                <?php echo form_open_multipart('dashboard/teacher_store'); ?>
                <div class="img-preview d-flex">
                    <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                </div>
                <?= $this->session->flashdata('message'); ?>
                <fieldset>
                    <?= validation_errors() ?>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="photo">Photo</label>
                            <input required type="file" id="gambar" class="form-control" size="20" name="photo" id="photo" placeholder="Masukan photo">
                            <span class="text-danger">
                                <?= form_error('photo') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="full_name">full_Name</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('full_name') ?>" name="full_name" id="full_name" aria-describedby="full_name" placeholder="Masukan Nama" required>
                            <span class="text-danger">
                                <?= form_error('full_name') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('username') ?>" name="username" id="username" aria-describedby="username" placeholder="Masukan Username" required>
                            <span class="text-danger">
                                <?= form_error('username') ?>
                            </span>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input autocomplete="off" type="password" class="form-control" required name="password" id="password" aria-describedby="password" placeholder="Masukan Password">
                            <span class="text-danger">
                                <?= form_error('password') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="password_confirmation">Password Confirmation</label>
                            <input autocomplete="off" type="password_confirmation" class="form-control" required name="password_confirmation" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Masukan Password Confirmation">
                            <span class="text-danger">
                                <?= form_error('password_confirmation') ?>
                            </span>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group">
                            <label for="nuptk">NUPTK</label>
                            <input autocomplete="off" type="number" class="form-control" value="<?= set_value('nuptk') ?>" name="nuptk" id="nuptk" aria-describedby="nuptk" placeholder="Masukan NUPTK" required>
                            <span class="text-danger">
                                <?= form_error('nuptk') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="birth_date">Birth Date</label>
                            <input autocomplete="off" type="date" class="form-control" value="<?= set_value('birth_date') ?>" name="birth_date" id="birth_date" aria-describedby="birth_date" placeholder="Masukan Tanggal Lahir" required>
                            <span class="text-danger">
                                <?= form_error('birth_date') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="birth_place">Birth Place</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('birth_place') ?>" name="birth_place" id="birth_place" aria-describedby="birth_place" placeholder="Masukan Tempat Lahir" required>
                            <span class="text-danger">
                                <?= form_error('birth_place') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select class="form-select" name="gender" id="gender" required>
                                <option value="" disabled selected>Pilih Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                            <span class="text-danger">
                                <?= form_error('gender') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="degree">Degree</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('degree') ?>" name="degree" id="degree" aria-describedby="degree" placeholder="Masukan Degree" required>
                            <span class="text-danger">
                                <?= form_error('degree') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Masukan Alamat" required><?= set_value('address') ?></textarea>
                            <span class="text-danger">
                                <?= form_error('address') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('phone_number') ?>" name="phone_number" id="phone_number" aria-describedby="phone_number" placeholder="Masukan Nomor Telepon" required>
                            <span class="text-danger">
                                <?= form_error('phone_number') ?>
                            </span>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group">
                            <label for="class_id">Class</label>
                            <select class="form-control" name="class_id" id="class_id">
                                <option value="">Select Class</option>
                                <?php foreach ($classes as $class) : ?>
                                    <option value="<?= $class['id_class'] ?>" <?= set_select('class_id', $class['id_class']) ?>>
                                        <?= $class['class_name'] ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="text-danger">
                                <?= form_error('class_id') ?>
                            </span>
                        </div>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                    </div>
                </fieldset>

                <?= form_close() ?>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>