<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">



        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                    <a href="<?= base_url('/dashboard/student_create') ?>" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

            <div class="panel-body table-responsive">
                <table style="width: 100%;" id="data-table-export" class="table table-striped table-bordered align-middle">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Masuk</th>
                            <th>Waktu Masuk</th>
                            <th>Keluar</th>
                            <th>Waktu Keluar</th>
                            <th>Status</th>
                            <th>Ket</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($attendances as $attendance) : ?>
                            <tr>
                                <td><?= ++$no; ?></td>
                                <td><img src="<?= $attendance['photo'] ?>" width="30" alt=""></td>
                                <td><?= $attendance['full_name'] ?></td>
                                <td><?= $attendance['masuk'] ? 'Ya' : 'Tidak' ?></td>
                                <td><?= $attendance['waktu_masuk'] ?></td>
                                <td><?= $attendance['keluar'] ? 'Ya' : 'Tidak' ?></td>
                                <td><?= $attendance['waktu_keluar'] ?></td>
                                <td><?= $attendance['status_hadir'] ?></td>
                                <td><?= $attendance['ket'] ?></td>
                                <td><?= $attendance['date'] ?></td>
                                <td>
                                    <a class="btn-sm btn bg-warning text-white" href="<?= base_url('dashboard/attendance_edit/' . $attendance['id_attendance']) ?>"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="myModalLabel">Download PDF</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>
         <div class="modal-body">
            <form action="<?= base_url('dashboard/generate_pdf') ?>" method="post" id="downloadForm">
               <div class="form-group">
                  <label for="guru">Guru:</label>
                  <select name="teacher" class="form-control" id="guru">
                     <option value="">Pilih Guru</option>
                     <?php foreach ($teachers as $teacher) : ?>
                        <option value="<?= $teacher['id_teacher'] ?>"><?= $teacher['full_name'] ?></option>
                     <?php endforeach; ?>
                  </select>
               </div>
               <div class="form-group">
                  <label for="kelas">Kelas:</label>
                  <select name="class" class="form-control" id="kelas">
                     <option value="">Pilih Kelas</option>
                     <?php foreach ($classes as $class) : ?>
                        <option value="<?= $class['id_class'] ?>"><?= $class['class_name'] ?></option>
                     <?php endforeach; ?>
                  </select>
               </div>
               <div class="form-group">
                  <label for="mapel">Mapel:</label>
                  <select name="subject" class="form-control" id="mapel">
                     <option value="">Pilih Mapel</option>
                     <?php foreach ($subjects as $subject) : ?>
                        <option value="<?= $subject['id_subject'] ?>"><?= $subject['name'] ?></option>
                     <?php endforeach; ?>
                  </select>
               </div>
               <div class="form-group">
                  <label for="tanggal">Tanggal:</label>
                  <input type="date" name="tanggal" class="form-control" id="tanggal">
               </div>
               <input type="submit" class="btn btn-primary" value="Download PDF">
            </form>
         </div>
         <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
