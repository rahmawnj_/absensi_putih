<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>

                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">
                <form action="<?= base_url('dashboard/attendance_update/' . $attendance['id_attendance']) ?>" method="post">
                    <?= $this->session->flashdata('message'); ?>
                    <fieldset>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="masuk">Masuk</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('masuk', $attendance['masuk']) ?>" name="masuk" id="masuk" aria-describedby="masuk" placeholder="Enter Masuk" required>
                                <span class="text-danger">
                                    <?= form_error('masuk') ?>
                                </span>
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="form-group">
                                <label for="waktu_masuk">Waktu Masuk</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('waktu_masuk', $attendance['waktu_masuk']) ?>" name="waktu_masuk" id="waktu_masuk" aria-describedby="waktu_masuk" placeholder="Enter Waktu Masuk" required>
                                <span class="text-danger">
                                    <?= form_error('waktu_masuk') ?>
                                </span>
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="form-group">
                                <label for="keluar">Keluar</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('keluar', $attendance['keluar']) ?>" name="keluar" id="keluar" aria-describedby="keluar" placeholder="Enter Keluar">
                                <span class="text-danger">
                                    <?= form_error('keluar') ?>
                                </span>
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="form-group">
                                <label for="waktu_keluar">Waktu Keluar</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('waktu_keluar', $attendance['waktu_keluar']) ?>" name="waktu_keluar" id="waktu_keluar" aria-describedby="waktu_keluar" placeholder="Enter Waktu Keluar">
                                <span class="text-danger">
                                    <?= form_error('waktu_keluar') ?>
                                </span>
                            </div>
                        </div>

                        <!-- Add other fields for attendance editing -->

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </fieldset>
                </form>


            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>