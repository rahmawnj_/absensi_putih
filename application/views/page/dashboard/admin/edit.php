<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                  
                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">
                <?php echo form_open_multipart('dashboard/admin_update/' . $admin['id_admin']); ?>
                <div class="img-preview d-flex">
                    <img src="<?= base_url('/assets/uploads/img/' . $admin['photo']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                </div>
                <?= $this->session->flashdata('message'); ?>
                <fieldset>

                    <div class="mb-3">
                        <div class="form-group">
                            <label for="photo">Photo</label>
                            <input type="file" id="gambar" class="form-control" size="20" name="photo" id="photo" placeholder="Masukan photo">
                            <span class="text-danger">
                                <?= form_error('photo') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('name', $admin['name']) ?>" name="name" id="name" aria-describedby="name" placeholder="Masukan Nama" required>
                            <span class="text-danger">
                                <?= form_error('name') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input autocomplete="off" type="text" class="form-control" value="<?= set_value('username', $admin['username']) ?>" name="username" id="username" aria-describedby="username" placeholder="Masukan Username">
                            <span class="text-danger">
                                <?= form_error('username') ?>
                            </span>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input autocomplete="off" type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Masukan Password">
                            <span class="text-danger">
                                <?= form_error('password') ?>
                            </span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="password_confirmation">Password_confirmation</label>
                            <input autocomplete="off" type="password_confirmation" class="form-control" name="password_confirmation" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Masukan Password_confirmation">
                            <span class="text-danger">
                                <?= form_error('password_confirmation') ?>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                    </div>
                </fieldset>
                <?= form_close() ?>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>