<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                    <a href="<?= base_url('/dashboard/subject_create') ?>" class="btn btn-primary float-right"><i class="fa fa-plus"></i></a>

                </div>
            </div>
            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
            <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

            <div class="panel-body table-responsive">
                <table style="width: 100%;" id="data-table-default" class="table table-striped table-bordered align-middle">
                    <thead>
                        <tr>
                            <th width="10%">#</th>
                            <th>Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($subjects as $subject) : ?>
                            <tr>
                                <td><?= ++$no; ?></td>

                                <td><?= $subject['name'] ?></td>
                                <td>
                                    <a class="btn-sm btn bg-warning text-white" href="<?= base_url('dashboard/subject_edit/' . $subject['id_subject']) ?>"><i class="fa fa-edit"></i></a>
                                    <a id="delete-button" class="btn-sm btn bg-danger text-white" href="<?= base_url('dashboard/subject_delete/' . $subject['id_subject']) ?>"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>