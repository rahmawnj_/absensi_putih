<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>

                </div>
            </div>
            <!-- END panel-heading -->
            <!-- BEGIN panel-body -->
            <div class="panel-body">
                <form action="<?= base_url('dashboard/subject_update/' . $subject['id_subject']) ?>" method="post">
                    <?= $this->session->flashdata('message'); ?>
                    <fieldset>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="name">subject Name</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('name', $subject['name']) ?>" name="name" id="name" aria-describedby="name" placeholder="Enter subject Name" required>
                                <span class="text-danger">
                                    <?= form_error('name') ?>
                                </span>
                            </div>
                        </div>

                    
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                        </div>
                    </fieldset>
                </form>


            </div>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>