<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>

    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>


    <div id="content" class="app-content">

        <div class="row">
            <div class="col-xl-4">
                <div class="widget widget-stats bg-blue">
                    <div class="stats-icon"><i class="fas fa-users"></i></div>
                    <div class="stats-info">
                        <h4>TOTAL SISWA</h4>
                        <p><?= $totalSiswa ?></p>
                    </div>
                    <div class="stats-link">
                        <a href="javascript:;">View Detail <i class="fas fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="widget widget-stats bg-info">
                    <div class="stats-icon"><i class="fas fa-sign-in-alt"></i></div>
                    <div class="stats-info">
                        <h4>TOTAL SISWA MASUK</h4>
                        <p><?= $jumlahSiswaMasuk ?></p>
                    </div>
                    <div class="stats-link">
                        <a href="javascript:;">View Detail <i class="fas fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="widget widget-stats bg-orange">
                    <div class="stats-icon"><i class="fas fa-sign-out-alt"></i></div>
                    <div class="stats-info">
                        <h4>TOTAL SISWA KELUAR</h4>
                        <p><?= $jumlahSiswaKeluar ?></p>
                    </div>
                    <div class="stats-link">
                        <a href="javascript:;">View Detail <i class="fas fa-arrow-alt-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="panel panel-inverse" data-sortable-id="index-1">
                    <div class="panel-heading">
                        <h4 class="panel-title">Grafik Kehadiran Siswa</h4>
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <div class="panel-body pe-1">
                        <div id="attendance-chart" class="h-300px"></div>
                    </div>
                </div>

            </div>
        </div>

        <!-- <form action="<?= base_url('dashboard/setting_update') ?>" method="post">
            <div class="form-group">
                <label for="waktu_masuk_awal">Waktu Masuk</label>
                <div class="input-group">
                    <input type="time" class="form-control" name="waktu_masuk_awal" value="<?= set_value('waktu_masuk_awal', explode('-', $waktuMasuk)[0]) ?>" required>
                    <span class="input-group-text">-</span>
                    <input type="time" class="form-control" name="waktu_masuk_akhir" value="<?= set_value('waktu_masuk_akhir', explode('-', $waktuMasuk)[1]) ?>" required>
                </div>
                <span class="text-danger"><?= form_error('waktu_masuk_awal') ?></span>
                <span class="text-danger"><?= form_error('waktu_masuk_akhir') ?></span>
            </div>

            <div class="form-group">
                <label for="waktu_keluar_awal">Waktu Keluar</label>
                <div class="input-group">
                    <input type="time" class="form-control" name="waktu_keluar_awal" value="<?= set_value('waktu_keluar_awal', explode('-', $waktuKeluar)[0]) ?>" required>
                    <span class="input-group-text">-</span>
                    <input type="time" class="form-control" name="waktu_keluar_akhir" value="<?= set_value('waktu_keluar_akhir', explode('-', $waktuKeluar)[1]) ?>" required>
                </div>
                <span class="text-danger"><?= form_error('waktu_keluar_awal') ?></span>
                <span class="text-danger"><?= form_error('waktu_keluar_akhir') ?></span>
            </div>

            <div class="form-group">
                <label for="telat">Telat</label>
                <input type="time" class="form-control" name="telat" value="<?= set_value('telat', $telat) ?>" required>
                <span class="text-danger"><?= form_error('telat') ?></span>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form> -->



        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>

<script>
    var seriesData = <?= $series ?>;
    var categoriesData = <?= $categories ?>;

    // Mengkonversi nilai series menjadi bilangan bulat
    var series = seriesData.map(function(item) {
        return {
            name: item.name,
            data: item.data.map(function(value) {
                return parseInt(value);
            })
        };
    });

    // Mengkonversi nilai categories menjadi bilangan bulat
    var categories = categoriesData.map(function(value) {
        return parseInt(value);
    });

    var chart = new ApexCharts(
        document.querySelector('#attendance-chart'), {
            chart: {
                height: 350,
                type: 'line',
                shadow: {
                    enabled: true,
                    color: COLOR_DARK,
                    top: 18,
                    left: 7,
                    blur: 10,
                    opacity: 1
                },
                toolbar: {
                    show: false
                }
            },
            title: {
                text: 'Grafik Kehadiran Siswa',
                align: 'center'
            },
            colors: [COLOR_BLUE, COLOR_TEAL],
            dataLabels: {
                enabled: true
            },
            stroke: {
                curve: 'smooth',
                width: 3
            },
            series: series,
            grid: {
                row: {
                    colors: [COLOR_SILVER_TRANSPARENT_1, 'transparent'],
                    opacity: 0.5
                },
            },
            markers: {
                size: 4
            },
            xaxis: {
                categories: categories,
                axisBorder: {
                    show: true,
                    color: COLOR_SILVER_TRANSPARENT_5,
                    height: 1,
                    width: '100%',
                    offsetX: 0,
                    offsetY: -1
                },
                axisTicks: {
                    show: true,
                    borderType: 'solid',
                    color: COLOR_SILVER,
                    height: 6,
                    offsetX: 0,
                    offsetY: 0
                }
            },
            legend: {
                show: true,
                position: 'top',
                offsetY: -10,
                horizontalAlign: 'right',
                floating: true
            }
        }
    );

    chart.render();
</script>