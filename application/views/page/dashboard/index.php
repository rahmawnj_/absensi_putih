<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content d-flex flex-column align-items-center justify-content-center">
        <div class="text-center mb-5">
            <h1>Welcome!</h1>
        </div>
        <?php $this->load->view('layouts/dashboard/footbar') ?>

    </div>

    <style>
        body {
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    </style>


</div>
<?php $this->load->view('layouts/dashboard/foot') ?>