<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="icon" href="<?= base_url('assets/logo.png') ?>">
	<!-- ================== BEGIN core-css ================== -->
	<link href="<?= base_url('assets/dashboard/css/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/css/facebook/app.min.css') ?>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->

	<link href="<?= base_url('assets/dashboard/plugins/select2/dist/css/select2.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" />

</head>

<body style="background-image: url('<?= base_url('/assets/uploads/img/' . $home_background['value']) ?>');
			background-size: cover;">
	<main class="d-flex w-100 h-100">
		<div class="container d-flex flex-column">
			<div class="row vh-100">
				<div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
					<div class="d-table-cell align-middle">
						<!-- <div class="card"> -->
						<div class="card-body text-center">
							<h3 class="card-title">Welcome Sistem Absensi Siswa otomatis</h3>
							<h3 class="card-title">Smart Card Berbasis Web</h3>
							<div class="row mt-5">
								<div class="col">
									<a href="<?= base_url('auth/login') ?>" class="btn btn-lg btn-primary btn-block">Dashboard</a>
								</div>
								<div class="col">
									<a href="<?= base_url('welcome/rekapan_kehadiran') ?>" class="btn btn-lg btn-secondary btn-block">Rekapan Kehadiran</a>
								</div>
							</div>
						</div>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</main>
	<!-- ================== BEGIN core-js ================== -->
	<script src="<?= base_url('/assets/dashboard/js/vendor.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/js/app.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/js/theme/facebook.min.js') ?>"></script>
	<!-- ================== END core-js ================== -->

	<script src="<?= base_url('/assets/dashboard/plugins/select2/dist/js/select2.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/sweetalert/dist/sweetalert.min.js') ?>"></script>

	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.colVis.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/pdfmake/build/pdfmake.min.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/pdfmake/build/vfs_fonts.js') ?>"></script>
	<script src="<?= base_url('/assets/dashboard/plugins/jszip/dist/jszip.min.js') ?>"></script>
	<script>
		$(".option-name").select2();
	</script>
	<script>
		const gambar = document.getElementById('gambar')
		$(gambar).change(evt => {
			const [file] = gambar.files
			if (file) {
				gmbr.src = URL.createObjectURL(file)
			}
		})


		const flashDataError = $('.flash-data-error').data('flashdataerror')
		if (flashDataError) {
			swal({
				title: "Gagal!",
				text: flashDataError,
				icon: "error",
			});
		}

		const flashDataSuccess = $('.flash-data-success').data('flashdatasuccess')
		if (flashDataSuccess) {
			swal({
				title: "Berhasil!",
				text: flashDataSuccess,
				icon: "success",
			});
		}

		const flashDataWarning = $('.flash-data-warning').data('flashdatawarning')
		if (flashDataWarning) {
			swal({
				title: "Peringatan!",
				text: flashDataWarning,
				icon: "warning",
			});
		}

		$('#data-table-default').DataTable({
			responsive: true,
			dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
			buttons: [],
		});
	</script>

	<script>
		$('#data-table-report').DataTable({
			responsive: true,
			dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
			buttons: [{
				extend: 'excel',
				className: 'btn-sm',
				text: "Export"
			}, ],
		});
		$('#data-table-export').DataTable({
			dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
			buttons: [{
				extend: 'excel',
				className: 'btn-sm',
				text: "Download Excel",
				title: null
			}],
			bInfo: false // opsi ini akan menghilangkan info judul tabel
		});
	</script>
</body>

</html>