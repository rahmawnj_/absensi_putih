<!-- ================== BEGIN core-js ================== -->
<script src="<?= base_url('/assets/dashboard/js/vendor.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/js/app.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/js/theme/facebook.min.js') ?>"></script>
<!-- ================== END core-js ================== -->

<script src="<?= base_url('/assets/dashboard/plugins/select2/dist/js/select2.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/sweetalert/dist/sweetalert.min.js') ?>"></script>

<script src="<?= base_url('/assets/dashboard/plugins/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.colVis.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/pdfmake/build/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/pdfmake/build/vfs_fonts.js') ?>"></script>
<script src="<?= base_url('/assets/dashboard/plugins/jszip/dist/jszip.min.js') ?>"></script>

<script>
	$(".option-name").select2();
</script>
<script>
	const gambar = document.getElementById('gambar')
	$(gambar).change(evt => {
		const [file] = gambar.files
		if (file) {
			gmbr.src = URL.createObjectURL(file)
		}
	})


	const flashDataError = $('.flash-data-error').data('flashdataerror')
	if (flashDataError) {
		swal({
			title: "Gagal!",
			text: flashDataError,
			icon: "error",
		});
	}

	const flashDataSuccess = $('.flash-data-success').data('flashdatasuccess')
	if (flashDataSuccess) {
		swal({
			title: "Berhasil!",
			text: flashDataSuccess,
			icon: "success",
		});
	}

	const flashDataWarning = $('.flash-data-warning').data('flashdatawarning')
	if (flashDataWarning) {
		swal({
			title: "Peringatan!",
			text: flashDataWarning,
			icon: "warning",
		});
	}

	$('#data-table-default').DataTable({
		responsive: true,
		dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
		buttons: [],
	});
</script>

<script>
	$('#data-table-report').DataTable({
		responsive: true,
		dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
		buttons: [{
			extend: 'excel',
			className: 'btn-sm',
			text: "Export"
		}, ],
	});
	$('#data-table-export').DataTable({
		dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
		buttons: [{
			extend: 'excel',
			className: 'btn-sm',
			text: "Download Excel",
			title: null
		}],
		bInfo: false // opsi ini akan menghilangkan info judul tabel
	});
</script>
</body>

</html>