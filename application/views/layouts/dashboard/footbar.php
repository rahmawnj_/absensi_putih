<footer id="footer" class="app-footer mx-0 px-0 text-center">
  
    <div class="d-flex justify-content-center" id="current-time"></div>
</footer>

<script>
    // Mendapatkan elemen waktu saat ini
    var currentTime = document.getElementById("current-time");

    // Mengupdate waktu saat ini setiap detik
    setInterval(function() {
        var currentDate = new Date();
        currentTime.innerText = currentDate.toLocaleTimeString();
    }, 1000);
</script>
