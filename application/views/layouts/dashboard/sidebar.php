<div id="sidebar" class="app-sidebar" style="background-color: #eff2f5;">
	<!-- BEGIN scrollbar -->
	<div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
		<!-- BEGIN menu -->
		<div class="menu">
			<div class="menu-profile">

				<a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile" data-target="#appSidebarProfileMenu">
					<div class="menu-profile-cover with-shadow"></div>
					<div class="menu-profile-image d-flex justify-content-center align-items-center">
						<img style="height: 100%; width:100%;" src="<?= base_url('/assets/uploads/img/' . $this->session->userdata('photo')) ?>" alt="" />
					</div>

					<div class="menu-profile-info">
						<div class="d-flex align-items-center">
							<div class="flex-grow-1">
								<?= $this->session->userdata('name') ?>
							</div>
							<div class="menu-caret ms-auto"></div>
						</div>
						<small><?= $this->session->userdata('role') ?></small>
					</div>
				</a>

			</div>
			<div id="appSidebarProfileMenu" class="collapse">
				<div class="menu-item pt-5px <?= ($title == 'Setting' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/setting') ?>" class="menu-link">
						<div class="menu-icon"><i class="fa fa-cog"></i></div>
						<div class="menu-text">Settings</div>
					</a>
				</div>
				<div class="menu-divider m-0"></div>
			</div>
			<div class="menu-header">Navigation</div>
			<div class="menu-item <?= ($title == 'Profile' ? 'active' : '') ?>">
				<a href="<?= base_url('auth/profile') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-user-circle"></i>
					</div>
					<div class="menu-text">Profile</div>
				</a>
			</div>
			<?php if ($this->session->userdata('role') == 'admin') : ?>
				<div class="menu-item <?= ($title == 'Admin' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/admin_index') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-user-shield"></i>
						</div>
						<div class="menu-text">Admin</div>
					</a>
				</div>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin') :  ?>
				<div class="menu-item <?= ($title == 'Teacher' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/teacher_index') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-chalkboard-teacher"></i>
						</div>
						<div class="menu-text">Data Guru</div>
					</a>
				</div>
			<?php endif ?>

			<div class="menu-item <?= ($title == 'Student' ? 'active' : '') ?>">
				<a href="<?= base_url('dashboard/student_index') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-user-graduate"></i>
					</div>
					<div class="menu-text">Data Siswa</div>
				</a>
			</div>

			<?php if ($this->session->userdata('role') == 'admin') :  ?>
				<div class="menu-item <?= ($title == 'Holiday' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/holiday_index') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-calendar"></i>
						</div>
						<div class="menu-text">Holiday</div>
					</a>
				</div>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin') :  ?>
				<div class="menu-item <?= ($title == 'Class' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/class_index') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-chalkboard"></i>
						</div>
						<div class="menu-text">Kelas</div>
					</a>
				</div>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'admin') : ?>
				<div class="menu-item <?= ($title == 'Subject' ? 'active' : '') ?>">
					<a href="<?= base_url('dashboard/subject_index') ?>" class="menu-link">
						<div class="menu-icon">
							<i class="fas fa-book"></i>
						</div>
						<div class="menu-text">Mata Pelajaran</div>
					</a>
				</div>
			<?php endif ?>

			<div class="menu-item <?= ($title == 'Rekapan Kehadiran' ? 'active' : '') ?>">
				<a href="<?= base_url('dashboard/rekapan_kehadiran') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-clipboard-list"></i>
					</div>
					<div class="menu-text">Rekapan Kehadiran</div>
				</a>
			</div>
			<div class="menu-item <?= ($title == 'Scan RFID' ? 'active' : '') ?>">
				<a href="<?= base_url('dashboard/scan_rfid') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-id-card"></i>
					</div>
					<div class="menu-text">Scan Smart Card</div>
				</a>
			</div>
			<div class="menu-item">
				<a href="<?= base_url('dashboard/monitoring_waktu') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-clock"></i>
					</div>
					<div class="menu-text">Monitoring waktu</div>
				</a>
			</div>
			<div class="menu-item mt-5">
				<a href="<?= base_url('auth/logout') ?>" class="menu-link">
					<div class="menu-icon">
						<i class="fas fa-sign-out-alt"></i>
					</div>
					<div class="menu-text">Logout</div>
				</a>
			</div>

		</div>
		<!-- END menu -->
	</div>
	<!-- END scrollbar -->
</div>
<div class="app-sidebar-bg"></div>
<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a></div>
<!-- END #sidebar -->