<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>SCHOOL | <?= $title ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link rel="icon" href="<?= base_url('assets/logo.png') ?>">
	<!-- ================== BEGIN core-css ================== -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/css/vendor.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/css/google/app.min.css') ?>" rel="stylesheet" />
	<!-- ================== END core-css ================== -->

	<link href="<?= base_url('assets/dashboard/plugins/select2/dist/css/select2.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/dashboard/plugins/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') ?>" rel="stylesheet" />


</head>

<body>