<div id="header" class="app-header" style="background-color: #e0e5eb;">
	<!-- BEGIN navbar-header -->
	<div class="navbar-header">
		<button type="button" class="navbar-desktop-toggler" data-toggle="app-sidebar-minify">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a href="index.html" class="navbar-brand">
			<b class="me-1"><?= $title ?? 'Dashboard' ?></b>
		</a>
		<button type="button" class="navbar-mobile-toggler" data-toggle="app-sidebar-mobile">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<!-- END navbar-header -->
	<!-- BEGIN header-nav -->
	<div class="navbar-nav">
		<div class="navbar-item navbar-form"></div>
		<!-- <div class="navbar-item dropdown">
			<a href="#" data-bs-toggle="dropdown" class="navbar-link dropdown-toggle icon">
				<i class="fa fa-bell"></i>
				<span class="badge">5</span>
			</a>
			<div class="dropdown-menu media-list dropdown-menu-end">
				<div class="dropdown-header">Notifications (5)</div>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-bug media-object bg-gray-400"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">Server Error Reports <i class="fa fa-exclamation-circle text-danger"></i></h6>
						<div class="text-muted fs-12px">3 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<img src="../assets/img/user/user-1.jpg" class="media-object" alt="" />
						<i class="fab fa-facebook-messenger text-blue media-object-icon"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">John Smith</h6>
						<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
						<div class="text-muted fs-12px">25 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<img src="../assets/img/user/user-2.jpg" class="media-object" alt="" />
						<i class="fab fa-facebook-messenger text-blue media-object-icon"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading">Olivia</h6>
						<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
						<div class="text-muted fs-12px">35 minutes ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-plus media-object bg-gray-400"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading"> New User Registered</h6>
						<div class="text-muted fs-12px">1 hour ago</div>
					</div>
				</a>
				<a href="javascript:;" class="dropdown-item media">
					<div class="media-left">
						<i class="fa fa-envelope media-object bg-gray-400"></i>
						<i class="fab fa-google text-warning media-object-icon fs-14px"></i>
					</div>
					<div class="media-body">
						<h6 class="media-heading"> New Email From John</h6>
						<div class="text-muted fs-12px">2 hour ago</div>
					</div>
				</a>
				<div class="dropdown-footer text-center">
					<a href="javascript:;" class="text-decoration-none">View more</a>
				</div>
			</div>
		</div> -->
		<div class="navbar-item navbar-user dropdown">
			<a href="#" class="navbar-link dropdown-toggle d-flex align-items-center" data-bs-toggle="dropdown">
				<img src="<?= base_url('/assets/uploads/img/' . $this->session->userdata('photo')) ?>" alt="" />
				<span class="d-none d-md-inline"><?= $this->session->userdata('name') ?></span> <b class="caret ms-lg-2"></b>
			</a>
			<div class="dropdown-menu dropdown-menu-end me-1">
				<a href="<?= base_url('auth/profile') ?>" class="dropdown-item">Edit Profile</a>
				<div class="dropdown-divider"></div>
				<a href="<?= base_url('auth/logout') ?>" class="dropdown-item">Log Out</a>
			</div>
		</div>
	</div>
	<!-- END header-nav -->
</div>