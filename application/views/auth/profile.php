<?php $this->load->view('layouts/dashboard/head') ?>

<div id="app" class="app app-header-fixed app-sidebar-fixed app-with-wide-sidebar app-with-light-sidebar">

    <?php $this->load->view('layouts/dashboard/headbar') ?>
    <?php $this->load->view('layouts/dashboard/sidebar') ?>
    <div id="content" class="app-content">


        <div class="panel panel-inverse">
            <!-- BEGIN panel-heading -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= $title ?></h4>
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>

                </div>
            </div>
            <!-- END panel-heading -->
            <?php if ($this->session->userdata('role') == 'admin') : ?>
                <div class="panel-body">
                    <?= form_open_multipart('auth/updateProfile/' . $user['id_admin']); ?>
                    <div class="img-preview d-flex">
                        <img src="<?= base_url('/assets/uploads/img/' . $user['photo']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                    </div>
                    <?= $this->session->flashdata('message'); ?>
                    <fieldset>
                        <input type="hidden" name="role" value="admin">
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="photo">Foto</label>
                                <input type="file" id="gambar" class="form-control" size="20" name="photo" id="photo" placeholder="Masukkan foto">
                                <span class="text-danger">
                                    <?= form_error('photo') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('name', $user['name']) ?>" name="name" id="name" aria-describedby="name" placeholder="Masukkan Nama" required>
                                <span class="text-danger">
                                    <?= form_error('name') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('username', $user['username']) ?>" name="username" id="username" aria-describedby="username" placeholder="Masukkan Username">
                                <span class="text-danger">
                                    <?= form_error('username') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input autocomplete="off" type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Masukkan Password">
                                <span class="text-danger">
                                    <?= form_error('password') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="password_confirmation">Konfirmasi Password</label>
                                <input autocomplete="off" type="password" class="form-control" name="password_confirmation" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Masukkan Konfirmasi Password">
                                <span class="text-danger">
                                    <?= form_error('password_confirmation') ?>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                        </div>
                    </fieldset>
                    <?= form_close() ?>
                </div>
            <?php endif ?>

            <?php if ($this->session->userdata('role') == 'teacher') : ?>
                <!-- form_teacher_profile.php -->
                <div class="panel-body">
                    <?= form_open_multipart('auth/updateTeacherProfile/' . $user['id_teacher']); ?>
                    <div class="img-preview d-flex">
                        <img src="<?= base_url('/assets/uploads/img/' . $user['photo']) ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:50px" alt="">
                    </div>
                    <?= $this->session->flashdata('message'); ?>
                    <fieldset>
                        <input type="hidden" name="role" value="teacher">
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="photo">Foto</label>
                                <input type="file" class="form-control" name="photo" id="photo" placeholder="Masukkan foto">
                                <span class="text-danger">
                                    <?= form_error('photo') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="full_name">Nama Lengkap</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('full_name', $user['full_name']) ?>" name="full_name" id="full_name" aria-describedby="full_name" placeholder="Masukkan Nama Lengkap" required>
                                <span class="text-danger">
                                    <?= form_error('full_name') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="nuptk">NUPTK</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('nuptk', $user['nuptk']) ?>" name="nuptk" id="nuptk" aria-describedby="nuptk" placeholder="Masukkan NUPTK" required>
                                <span class="text-danger">
                                    <?= form_error('nuptk') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('username', $user['username']) ?>" name="username" id="username" aria-describedby="username" placeholder="Masukkan Username">
                                <span class="text-danger">
                                    <?= form_error('username') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input autocomplete="off" type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Masukkan Password">
                                <span class="text-danger">
                                    <?= form_error('password') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="password_confirmation">Konfirmasi Password</label>
                                <input autocomplete="off" type="password" class="form-control" name="password_confirmation" id="password_confirmation" aria-describedby="password_confirmation" placeholder="Masukkan Konfirmasi Password">
                                <span class="text-danger">
                                    <?= form_error('password_confirmation') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="birth_date">Tanggal Lahir</label>
                                <input type="date" class="form-control" name="birth_date" value="<?= set_value('birth_date', $user['birth_date']) ?>" id="birth_date" placeholder="Masukkan Tanggal Lahir">
                                <span class="text-danger">
                                    <?= form_error('birth_date') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="birth_place">Tempat Lahir</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('birth_place', $user['birth_place']) ?>" name="birth_place" id="birth_place" aria-describedby="birth_place" placeholder="Masukkan Tempat Lahir">
                                <span class="text-danger">
                                    <?= form_error('birth_place') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="gender">Jenis Kelamin</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="Male" <?= set_select('gender', 'Male', $user['gender'] == 'Male') ?>>Laki-Laki</option>
                                    <option value="Female" <?= set_select('gender', 'Female', $user['gender'] == 'Female') ?>>Perempuan</option>
                                </select>
                                <span class="text-danger">
                                    <?= form_error('gender') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="degree">Gelar</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('degree', $user['degree']) ?>" name="degree" id="degree" aria-describedby="degree" placeholder="Masukkan Gelar">
                                <span class="text-danger">
                                    <?= form_error('degree') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea class="form-control" name="address" id="address" rows="3" placeholder="Masukkan Alamat"><?= set_value('address', $user['address']) ?></textarea>
                                <span class="text-danger">
                                    <?= form_error('address') ?>
                                </span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-group">
                                <label for="phone_number">Nomor Telepon</label>
                                <input autocomplete="off" type="text" class="form-control" value="<?= set_value('phone_number', $user['phone_number']) ?>" name="phone_number" id="phone_number" aria-describedby="phone_number" placeholder="Masukkan Nomor Telepon">
                                <span class="text-danger">
                                    <?= form_error('phone_number') ?>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100px me-5px">Submit</button>
                        </div>
                    </fieldset>
                    <?= form_close() ?>
                </div>
            <?php endif; ?>

        </div>

        <?php $this->load->view('layouts/dashboard/footbar') ?>
    </div>
</div>
<?php $this->load->view('layouts/dashboard/foot') ?>